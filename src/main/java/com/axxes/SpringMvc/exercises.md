# Exercise 1

We are given a controller returning a basic 'Hello World' view. We would like our controller to accessible
through a custom path, for example:

- /axxes
- /axxes/my-controller
- /axxes/any/path/I/want

This can be achieved by using a Servlet Configuration Bean or as we are using Spring Boot, Spring Boot properties can be
used as well.

### Tips:
- ConfigurableServletWebServerFactory
- server.servlet.path

---

# Exercise 2

We are given a controller returning a basic view based on a `Model` (we'll touch this so called Model later on).
We would like catch the requests going to and coming from the controller:

- Log a message containing the request path going to the controller.
- Log a message containing the controller Models attribute 'message'.
- Log a message when everything is finished.

### Tips:
- You will need an interceptor class. (look at the HandlerInterceptorAdapter interface)
- Do not forget to configure your interceptor.

---

# Exercise 3:

Playing around with Controllers and RequestMapping:

- Create a new controller.
- Add different request mappings (GET, POST, PUT, DELETE, ...) to the controller and map them individually to `/exercise-3`.
  Verify your mappings are working as intended, this can be done by logging a specific message for each mapping,
  by running your application in debug and using breakpoints, or any other method you see fit.

    - What happens if two mappings of the same type direct to the same endpoint?
    - What happens if two mappings of a different type direct to the same endpoint?



### Extra:

- Add the following mapping to your controller and check if you can reach it.:

````java
@GetMapping("/extra/exercise-3")
public String extra(final Model model) {
    model.addAttribute("message", "Extra exercise 3.");
    return "basicView";
}
````

- Extract the method/mapping to its own controller. Only this time you are not allowed to have any request mapping
  on individual methods in the controller.

---

# Exercise 4:

Passing variables to the endpoint:

- Create a new controller.
- Add the following methods to this controller:
    - POST accepting an optional String request parameter.
    - POST accepting a required integer request parameter.
    - GET accepting an integer path variable.

---

# Exercise 5:

Consumable & producible media types:

- Create a new controller.
- Add a method producing a 'simple object'.
- Add a method consuming a 'simple object' as JSON.

The 'simple object' is given.

### Tips:
- A "ResponseBody" might be required to return an object.

---

# Exercise 6:

Model all the things:

- Given a ModelController
- Add a method returning a plain model object.
- Add a method returning a ModelMap object.
- Add a method returning a ModelAndView object.

---

# Exercise 7:

We are given a controller which is making use of an exception service. Due to the nature of this service a lot
of exceptions are thrown. We would like for all these exceptions to be handled in the 'background' causing users not to
be confronted with our 'internal' exceptions.

- All types of exceptions should be handled by a general exception handler.
- If an exception is handled, inform the user something went wrong. You can use the 'basicView' we have been using
for previous exercises or try to create your own.
- IllegalArgumentExceptions should be handled by a specific exception handler.


### Extra:
- When you return a view to the user, send along a http 500 code instead of the default 200.
(Might require you to you a ResponseEntity / ResponseBody)

---

# Exercise 8:

We are again given a controller that is making use of an exception service. This time the controller already exception
handling going on.

- Rework the existing controller, allowing it to have external instead internal exception handling using a
  ControllerAdvice.


###Extra:
- Move the existing controller in a separate package (for example /exercise_8/first-controller).
- Create an additional controller who's method(s) return an exception as well, add this controller to its own
  package (for example /exercise_8/second-controller)
- Play around with the ControllerAdvice to only handle exceptions thrown from:
    1. only the first controller
    2. only the second controller
    3. all controllers

---

# Exercise 9:

We are again given a Person object. And would like to make a simple editor for this Person object.

- Create a controller allowing us to ask a detailed view of this person.
- Extend the controller allowing us to edit the persons details.
- When you submit your changes, show a summary of the persons details again.